-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema playlistdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema playlistdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `playlistdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `playlistdb` ;

-- -----------------------------------------------------
-- Table `playlistdb`.`genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`genres` (
  `d_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `d_pic` VARCHAR(400) NULL DEFAULT NULL,
  PRIMARY KEY (`d_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`albums`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`albums` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(90) NOT NULL,
  `d_cover` VARCHAR(400) NULL DEFAULT NULL,
  `d_release_date` DATE NULL DEFAULT NULL,
  `d_fans` INT(11) NOT NULL,
  `album_tracklist_url` VARCHAR(400) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  `d_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_albums_genres1_idx` (`genre_id` ASC) VISIBLE,
  CONSTRAINT `fk_albums_genres1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `playlistdb`.`genres` (`d_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`artists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`artists` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `d_pic` VARCHAR(400) NULL DEFAULT NULL,
  `name` VARCHAR(90) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  `tracklist_url` VARCHAR(500) NOT NULL,
  `d_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`artist_albums`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`artist_albums` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `artist_id` INT(11) NOT NULL,
  `album_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_artists_has_albums_albums1_idx` (`album_id` ASC) VISIBLE,
  INDEX `fk_artists_has_albums_artists1_idx` (`artist_id` ASC) VISIBLE,
  CONSTRAINT `fk_artists_has_albums_albums1`
    FOREIGN KEY (`album_id`)
    REFERENCES `playlistdb`.`albums` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_artists_has_albums_artists1`
    FOREIGN KEY (`artist_id`)
    REFERENCES `playlistdb`.`artists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`artist_genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`artist_genres` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `artist_id` INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_artists_has_genres_genres1_idx` (`genre_id` ASC) VISIBLE,
  INDEX `fk_artists_has_genres_artists1_idx` (`artist_id` ASC) VISIBLE,
  CONSTRAINT `fk_artists_has_genres_artists1`
    FOREIGN KEY (`artist_id`)
    REFERENCES `playlistdb`.`artists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_artists_has_genres_genres1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `playlistdb`.`genres` (`d_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `email` VARCHAR(80) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `picture` VARCHAR(45) NULL DEFAULT NULL,
  `is_deleted` TINYINT(4) NOT NULL DEFAULT 0,
  `role_id` INT(11) NOT NULL DEFAULT 2,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_users_roles1_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_roles1`
    FOREIGN KEY (`role_id`)
    REFERENCES `playlistdb`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`banned_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`banned_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `is_banned` TINYINT(4) NOT NULL DEFAULT 1,
  `banned_on` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `duration` DATE NOT NULL,
  `message` VARCHAR(300) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_banned_users_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_banned_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `playlistdb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`playlists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`playlists` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(90) NOT NULL,
  `duration` INT(11) NOT NULL,
  `rank` INT(11) NOT NULL,
  `start_point` VARCHAR(200) NOT NULL,
  `end_point` VARCHAR(200) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL DEFAULT 0,
  `playlist_picture` VARCHAR(400) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_playlists_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `playlistdb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`playlist_genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`playlist_genres` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` INT(11) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_playlists_has_genres_genres1_idx` (`genre_id` ASC) VISIBLE,
  INDEX `fk_playlists_has_genres_playlists1_idx` (`playlist_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_has_genres_genres1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `playlistdb`.`genres` (`d_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlists_has_genres_playlists1`
    FOREIGN KEY (`playlist_id`)
    REFERENCES `playlistdb`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`reactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`reactions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `reaction` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`playlist_reactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`playlist_reactions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `reaction_id` INT(11) NOT NULL,
  `playlist_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_playlist_reactions_reactions1_idx` (`reaction_id` ASC) VISIBLE,
  INDEX `fk_playlist_reactions_playlists1_idx` (`playlist_id` ASC) VISIBLE,
  INDEX `fk_playlist_reactions_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlist_reactions_playlists1`
    FOREIGN KEY (`playlist_id`)
    REFERENCES `playlistdb`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlist_reactions_reactions1`
    FOREIGN KEY (`reaction_id`)
    REFERENCES `playlistdb`.`reactions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlist_reactions_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `playlistdb`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`tokens` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`tracks` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(90) NOT NULL,
  `duration` INT(11) NOT NULL,
  `link` VARCHAR(400) NOT NULL,
  `rank` INT(11) NOT NULL,
  `preview_url` VARCHAR(400) NOT NULL,
  `genre_id` INT(11) NOT NULL,
  `album_id` INT(11) NOT NULL,
  `artist_id` INT(11) NOT NULL,
  `d_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tracks_genres1_idx` (`genre_id` ASC) VISIBLE,
  INDEX `fk_tracks_albums1_idx` (`album_id` ASC) VISIBLE,
  INDEX `fk_tracks_artists1_idx` (`artist_id` ASC) VISIBLE,
  CONSTRAINT `fk_tracks_albums1`
    FOREIGN KEY (`album_id`)
    REFERENCES `playlistdb`.`albums` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tracks_artists1`
    FOREIGN KEY (`artist_id`)
    REFERENCES `playlistdb`.`artists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tracks_genres1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `playlistdb`.`genres` (`d_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `playlistdb`.`track_lists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `playlistdb`.`track_lists` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` INT(11) NOT NULL,
  `track_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_playlists_has_tracks_tracks1_idx` (`track_id` ASC) VISIBLE,
  INDEX `fk_playlists_has_tracks_playlists1_idx` (`playlist_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_has_tracks_playlists1`
    FOREIGN KEY (`playlist_id`)
    REFERENCES `playlistdb`.`playlists` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_playlists_has_tracks_tracks1`
    FOREIGN KEY (`track_id`)
    REFERENCES `playlistdb`.`tracks` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
