import serviceErrors from './service-errors.js';
import fetch from 'node-fetch';
import { BING_MAPS_KEY, BING_MAPS_LOCATION_URL, BING_MAPS_DURATION_URL } from '../common/constants.js';
import userRoles from '../common/user-roles.js';

const getAllPlaylists = playlistsData => {
  return async () => {
    return await playlistsData.getAllPlaylists();
  };
};

const getTopThreePlaylists = playlistsData => {
  return async () => {
    return await playlistsData.getTopThree();
  };
};

const getAllPlaylistsAdmin = playlistsData => {
  return async () => {
    return await playlistsData.getAllPlaylistsAdmin();
  };
};

const getPlaylistById = playlistsData => {
  return async (id) => {
    const playlist = await playlistsData.getById(id);

    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    return { error: null, playlist };
  };
};

const getAllGenresByPlaylistId = playlistsData => {
  return async (id) => {
    const playlist = await playlistsData.getById(id);

    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    const genres = await playlistsData.getAllGenresByPlaylist(id);
    return { error: null, genres };
  };
};

const getAllTracksByPlaylistId = playlistsData => {
  return async (id) => {
    let playlist = null;
    id ? playlist = await playlistsData.getById(id) : null;

    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    const tracks = await playlistsData.getAllTracksByPlaylist(id);
    return { error: null, tracks };
  };
};


const getDuration = playlistsData => {
  return async (start_point, end_point) => {

    const getPoint = async (point) => {
      const { adminDistrict, locality, addressLine, number } = point;

      const pointLocation = await fetch(`${BING_MAPS_LOCATION_URL}&adminDistrict=${adminDistrict}&locality=${locality}&addressLine=${addressLine}%20${number}&key=${BING_MAPS_KEY}`)
        .then(res => res.json())
        .catch(error => {
          console.log(error);
        });


      return pointLocation.resourceSets[0].resources[0].geocodePoints.length === 1 ?
        pointLocation.resourceSets[0].resources[0].geocodePoints[0].coordinates :
        pointLocation.resourceSets[0].resources[0].geocodePoints.find(obj => obj.usageTypes[0] === 'route').coordinates;
    };

    const startPointCoordinates = await getPoint(start_point);

    const endPointCoordinates = await getPoint(end_point);

    const durationObject = await fetch(`${BING_MAPS_DURATION_URL}?origins=${startPointCoordinates[0]},${startPointCoordinates[1]}&destinations=${endPointCoordinates[0]},${endPointCoordinates[1]}&travelMode=driving&key=${BING_MAPS_KEY}`)
      .then(res => res.json())
      .catch(error => {
        console.log(error);
      });

    const duration = Math.round(durationObject.resourceSets[0].resources[0].results[0].travelDuration);

    const playlists = await playlistsData.getAllPlaylists();

    const reccomendedPlaylists = playlists.filter(playlist => playlist.duration / 60 < duration + 5 && playlist.duration / 60 > duration - 5);

    return { duration, reccomendedPlaylists };
  };
};


const createPlaylist = playlistsData => {
  return async (playlistData) => {
    const { start_point, end_point, user_id, duration, title, repeat_artists, genres, image } = playlistData;

    const existingPlaylist = await playlistsData.getPlaylistByTitle(title, user_id);

    if (existingPlaylist) {
      return { error: serviceErrors.DUPLICATE_RECORD, playlist: null };
    }

    const trackList = [];

    let playlistDuration = 0;

    if (genres[0].percentage === 0) {
      genres.forEach(genre => {
        genre.percentage = 100 / genres.length;
      });
    }

    for (const genre of genres) {
      const tracks = await playlistsData.getRandom(genre.genre_id);

      let currentDuration = 0;

      while (currentDuration < ((duration * (genre.percentage / 100) - 5) * 60) && currentDuration < ((duration * (genre.percentage / 100) + 5) * 60)) {
        const randomTrack = tracks[Math.floor(Math.random() * (tracks.length))];

        if (randomTrack.duration + currentDuration > (duration * (genre.percentage / 100) + 5) * 60) continue;

        if (!repeat_artists) {
          if (trackList.find(track => track.artist_id === randomTrack.artist_id)) continue;
        }

        if (!trackList.find(track => track.track_id === randomTrack.track_id)) {
          trackList.push(randomTrack);
          playlistDuration += randomTrack.duration;
          currentDuration += randomTrack.duration;
        }
      }
    }

    const genres_id = genres.map(genre => genre.genre_id);

    const rank = Math.round(trackList.reduce((acc, track) => {
      return acc + track.rank;
    }, 0) / (trackList.length));


    const playlist = await playlistsData.createPlaylist(trackList, title, playlistDuration, rank, start_point.locality, end_point.locality, genres_id, user_id, image);

    return { error: null, playlist: playlist };
  };
};

const updatePlaylist = playlistsData => {
  return async (id, title, user_id, role) => {
    const playlist = await playlistsData.getById(id);

    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    if (playlist.user_id !== user_id && userRoles.USER === role) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        playlist: null,
      };
    }

    const existingPlaylist = await playlistsData.getPlaylistByTitle(title, user_id);

    if (existingPlaylist) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        playlist: null,
      };
    }

    return { error: null, playlist: await playlistsData.update(id, title, user_id, role) };
  };
};

const removePlaylist = playlistsData => {
  return async (id, user_id, role) => {
    const playlist = await playlistsData.getById(id);

    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    if (playlist.user_id !== user_id && userRoles.USER === role) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        playlist: null,
      };
    }

    return { error: null, playlist: await playlistsData.remove(id) };
  };
};

const restorePlaylist = playlistsData => {
  return async (id, user_id, role) => {
    const playlist = await playlistsData.getById(id);

    if (!playlist) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        playlist: null,
      };
    }

    if (userRoles.USER === role) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        playlist: null,
      };
    }

    return { error: null, playlist: await playlistsData.restore(id) };
  };
};


export default {
  getAllPlaylists,
  getTopThreePlaylists,
  getPlaylistById,
  getAllGenresByPlaylistId,
  getAllTracksByPlaylistId,
  createPlaylist,
  updatePlaylist,
  removePlaylist,
  getDuration,
  restorePlaylist,
  getAllPlaylistsAdmin,
};