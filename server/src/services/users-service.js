import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import userRoles from '../common/user-roles.js';
import createToken from '../auth/create-token.js';

const getAllUsers = usersData => {
  return async () => {

    const allUsers = await usersData.getAllUsers();

    return allUsers;
  };
};

const createUser = (usersData) => {
  return async (data) => {
    const { username, password, email, first_name, last_name } = data;

    const existingUser = await usersData.getBy('username', username);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const existingEmail = await usersData.getBy('email', email);

    if (existingEmail) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const newUser = await usersData.createUser(userRoles.USER, username, await bcrypt.hash(password, 10), email, first_name, last_name);

    return { error: null, user: newUser };
  };
};

const loginUser = (usersData) => {
  return async (data) => {

    const { username, password } = data;

    const existingUser = await usersData.getByWithPassword('username', username);

    if (!existingUser || existingUser.is_deleted === 1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        token: null,
      };
    }


    if (!await bcrypt.compare(password, existingUser.password)) {
      return {
        error: serviceErrors.INVALID_PASSWORD,
        token: null,
      };
    }

    const token = createToken({
      user_id: existingUser.id,
      username: existingUser.username,
      email: existingUser.email,
      role_title: existingUser.role_title,
    });

    await usersData.loginUser(token);

    return { error: null, token: token };
  };
};

const logoutUser = usersData => {
  return async (token) => {
    await usersData.logoutUser(token);
  };
};

const updateUserProfile = usersData => {
  return async (userData) => {
    const { username, email, first_name, last_name, user_id } = userData;

    return await usersData.updateUser(username, email, first_name, last_name, user_id);
  };
};


const removeUser = usersData => {
  return async (id) => {
    const user = await usersData.getBy('id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return { error: null, user: await usersData.removeUser(id) };
  };
};

const restoreUser = usersData => {
  return async (id) => {
    const user = await usersData.getBy('id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return { error: null, user: await usersData.restoreUser(id) };
  };
};


export default {
  getAllUsers,
  createUser,
  loginUser,
  logoutUser,
  updateUserProfile,
  removeUser,
  restoreUser,
};