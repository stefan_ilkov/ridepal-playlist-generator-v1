export default {
  /** Such a record does not exist (when it is expected to exist) */
  RECORD_NOT_FOUND: 1,
  /** The requirements do not allow more than one of that resource */
  DUPLICATE_RECORD: 2,
  /** The requirements do not allow such an operation */
  OPERATION_NOT_PERMITTED: 3,
  /** Invalid password were provided for user login */
  INVALID_PASSWORD: 4,
  /** Invalid query parameters */
  INVALID_QUERY: 5,
  /** Username length is not in range */
  INVALID_USERNAME: 6,
};