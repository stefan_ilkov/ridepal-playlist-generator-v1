import serviceErrors from './service-errors.js';

const editUser = (adminData) => {
  return async (role_id, id) => {
    const user = await adminData.getBy('id', id);

    if (user.role_id === role_id) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        user: null,
      };
    }

    return { error: null, user: await adminData.editUser(role_id, id) };
  };
};

const deleteUser = adminData => {
  return async (id) => {
    const user = await adminData.getBy('user_id', id);

    if (!user || user.is_deleted === 1) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    const deletedUser = await adminData.removeUser(id);

    return { error: null, user: deletedUser };
  };
};

const banUser = adminData => {
  return async (banData) => {
    const { duration, message, user_id } = banData;

    if (!(await adminData.getBy('id', user_id))) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        banInfo: null,
      };
    }

    if (await adminData.getActiveBan(user_id)) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        banInfo: null,
      };
    }

    const banInfo = await adminData.addBan(message, new Date(duration), user_id);

    return { error: null, message: banInfo };
  };
};

const unbanUser = adminData => {
  return async (id) => {
    const user = await adminData.getBy('id', id);
    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        banInfo: null,
      };
    }

    const ban = await adminData.getActiveBan(id);

    if (!ban) {
      return {
        error: serviceErrors.OPERATION_NOT_PERMITTED,
        banInfo: null,
      };
    }

    const banInfo = await adminData.resetBanDate(id);
    return { error: null, banInfo: banInfo };
  };
};

const getUserById = adminData => {
  return async (id) => {
    const user = await adminData.getBy('id', id);
    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return { error: null, user: user };
  };
};

export default {
  editUser,
  getUserById,
  deleteUser,
  banUser,
  unbanUser,
};