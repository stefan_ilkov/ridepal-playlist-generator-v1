import dotenv from 'dotenv';

export const config = dotenv.config().parsed;
export const DB_CONFIG = {
  port: config.DBPORT,
  host: config.HOST,
  user: config.USER,
  password: config.PASSWORD,
  database: config.DATABASE,
};

export const PORT = 5555;