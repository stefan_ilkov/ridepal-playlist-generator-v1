export const TITLE_MIN_LENGTH = 2;
export const TITLE_MAX_LENGTH = 200;

export const BING_MAPS_KEY = 'AoqC_QG8e5Y8JcvFAR9gOtR7Dv53-6P8TglDxm6tvQPdzr65squZpL699fDFPEgg';

export const BING_MAPS_LOCATION_URL = 'http://dev.virtualearth.net/REST/v1/Locations?countryRegion=BG';

export const BING_MAPS_DURATION_URL = 'https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix';