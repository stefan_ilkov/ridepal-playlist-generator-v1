import db from './pool.js';

const getAll = async (sort, page, limit) => {
  const direction = sort || 'ASC';
  const resultsPerPage = limit || 200;
  const startIndex = page ? (page - 1) * limit : 0;

  const sql = `
    SELECT t.id as track_id, t.title, t.duration, t.link, t.rank, t.preview_url, t.genre_id, 
    t.album_id, t.artist_id, g.name as genre, g.d_pic as genre_pic, ar.name as artist, 
    ar.d_pic as artist_pic, al.title as album, al.d_cover as album_cover, 
    al.d_release_date as album_release_date, al.d_fans as album_fans
    FROM tracks t
    JOIN genres g ON t.genre_id = g.d_id
    JOIN artists ar ON t.artist_id = ar.id
    JOIN albums al ON t.album_id = al.id
    ORDER BY track_id ${direction}
    LIMIT ${startIndex}, ${resultsPerPage}
  `;

  return await db.query(sql);
};


export default {
  getAll,
};
