import db from './pool.js';

const deleteUser = async (id) => {
  const sql = `
  UPDATE users SET is_deleted = 1 WHERE user_id = ?
  `;
  await db.query(sql, [id]);

  return await getBy('user_id', id);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT u.id, u.role_id, u.username, u.first_name, u.last_name, u.email, u.is_deleted
  FROM users u
  WHERE u.${column} = ?
`;

  return (await db.query(sql, [value]))[0];
};

const addBan = async (message, duration, id) => {
  const sql = `
  INSERT INTO banned_users(message, duration, user_id)
  VALUES(?, ?, ?)
  `;
  const banId = await db.query(sql, [message, duration, id]);

  return (await db.query(`
  SELECT * FROM banned_users WHERE id = ?
  `, [banId.insertId]))[0];
};

const resetBanDate = async (id) => {
  const sql = `
  UPDATE banned_users SET is_banned = 0 WHERE user_id = ?
  `;

  return await db.query(sql, [id]);
};

const getActiveBan = async (user_id) => {
  return (await db.query(`
SELECT * FROM banned_users
WHERE user_id = ? AND is_banned = 1
`, [user_id]))[0];
};

const editUser = async (role_id, id) => {
  const sql = `
    UPDATE users
    SET role_id = ${role_id}
    WHERE id =  ${id}
  `;
  await db.query(sql);

  return await getBy('id', id);
};

export default {
  deleteUser,
  addBan,
  getBy,
  resetBanDate,
  getActiveBan,
  editUser,
};