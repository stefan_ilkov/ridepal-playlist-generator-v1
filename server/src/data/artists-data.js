import db from './pool.js';

const getAllArtists = async () => {
  const sql = `SELECT id, d_pic, name, d_id,
  (SELECT COUNT(albums.title) FROM albums JOIN artist_albums ON artist_albums.album_id = albums.id WHERE artist_albums.artist_id = artists.id) AS album_count
  FROM artists
  `;

  return await db.query(sql);
};

export default {
  getAllArtists,
};