import db from './pool.js';

const getAllUsers = async () => {
  const sql = `SELECT u.id, u.first_name, u.last_name, u.username, u.email, u.picture, u.is_deleted, r.role_title
  FROM users u
  JOIN roles r ON r.id=u.role_id
  `;
  return await db.query(sql);
};

const createUser = async (role, username, password, email, first_name, last_name) => {
  const sql = `
  INSERT INTO users (role_id, username, password, email, first_name, last_name)
  VALUES((SELECT id FROM roles WHERE role_title = '${role}'), ?, ?, ?, ?, ?)
  `;

  const user = await db.query(sql, [username, password, email, first_name, last_name]);

  return await getBy('id', user.insertId);
};

const getBy = async (column, value) => {
  const sql = `
  SELECT u.id, u.role_id, u.username, u.first_name, u.last_name, u.email, u.is_deleted
  FROM users u
  WHERE u.${column} = ?
`;

  return (await db.query(sql, [value]))[0];
};

const loginUser = async (token) => {
  const sql = `
  INSERT INTO tokens(token)
  VALUES (?)
  `;
  return await db.query(sql, [token]);
};

const logoutUser = async (token) => {
  const sql = `
  DELETE FROM tokens 
  WHERE token = '${token}'
  `;
  return await db.query(sql, [token]);
};

const getByWithPassword = async (column, value) => {
  const sql = `
  SELECT u.id, u.first_name, u.last_name, u.username, u.email, u.picture, u.is_deleted,u.password, r.role_title
  FROM users u
  JOIN roles r ON r.id=u.role_id
  WHERE ${column} = ?
`;

  return (await db.query(sql, [value]))[0];
};

const updateUser = async (username, email, first_name, last_name, user_id) => {
  const sql = `
    UPDATE users
    SET username = '${username}', email = '${email}', first_name = '${first_name}', last_name = '${last_name}'
    WHERE user_id =  ${user_id}
  `;
  await db.query(sql);

  return await getBy('user_id', user_id);
};

const removeUser = async (id) => {
  const sql = `
    UPDATE users 
    SET is_deleted = 1
    WHERE id = ?
  `;

  await db.query(sql, [id]);

  const sql2 = `
    SELECT * FROM users
    WHERE id = ?
`;

  const result = await db.query(sql2, [id]);
  return result[0];
};

const restoreUser = async (id) => {
  const sql = `
    UPDATE users 
    SET is_deleted = 0
    WHERE id = ${id}
  `;

  await db.query(sql);

  const sql2 = `
    SELECT * FROM users
    WHERE id = ?
`;

  const result = await db.query(sql2, [id]);
  return result[0];
};
export default {
  getAllUsers,
  createUser,
  getBy,
  loginUser,
  logoutUser,
  getByWithPassword,
  updateUser,
  removeUser,
  restoreUser,
};