import db from './pool.js';

const getAll = async (sort, page, limit) => {
  const direction = sort || 'ASC';
  const resultsPerPage = limit || 40;
  const startIndex = page ? (page - 1) * limit : 0;

  const sql = `
    SELECT d_id as genre_id, name as genre, d_pic as genre_pic
    FROM genres
    ORDER BY genre_id ${direction}
    LIMIT ${startIndex}, ${resultsPerPage}
  `;

  return await db.query(sql);
};

export default {
  getAll,
};