import jwt from 'jsonwebtoken';
import { config } from '../config.js';

const PRIVATE_KEY = config.PRIVATE_KEY;

const createToken = (payload) => {
  const token = jwt.sign(
    payload,
    PRIVATE_KEY,
  );

  return token;
};

export default createToken;
