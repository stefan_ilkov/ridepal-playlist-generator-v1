import passportJwt from 'passport-jwt';
import { config } from '../config.js';

const PRIVATE_KEY = config.PRIVATE_KEY;

const options = {
  secretOrKey: PRIVATE_KEY, // what the secret key is
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(), // where to extract the token from
};

// the payload will be injected from the decoded token
const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const user = {
    user_id: payload.user_id,
    username: payload.username,
    email: payload.email,
    role_title: payload.role_title,
  };

  // the user object will be injected in the request object and can be accessed as req.user in authenticated controllers
  done(null, user);
});

export default jwtStrategy;
