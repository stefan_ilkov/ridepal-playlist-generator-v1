import express from 'express';
import { authMiddleware } from '../middleware/auth.middleware.js';
import { roleAuthorization } from '../middleware/role-middleware.js';
import userRoles from '../common/user-roles.js';
import serviceErrors from '../services/service-errors.js';
import adminService from '../services/admin-service.js';
import adminData from '../data/admin-data.js';
import loggedUserMiddlware from '../middleware/logged-user-middleware.js';
import playlistsService from '../services/playlists-service.js';
import playlistsData from '../data/playlists-data.js';

const adminController = express.Router();

adminController.use(authMiddleware);
adminController.use(loggedUserMiddlware);
adminController.use(roleAuthorization(userRoles.ADMIN));

adminController
  // edit user
  .put('/user/:id', async (req, res) => {

    const { id } = req.params;

    if (req.user.user_id === +id) {
      return res.status(409).json({ error: 'You cannot change your role!' });
    }

    const { error, user } = await adminService.editUser(adminData)(req.body.role_id, +id);

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res.status(405).json({ error: 'User already has the current role!' });
    } else {
      res.status(201).json({ message: `User with id ${user.id} changed to ${user.role_id === 1 ? 'Admin' : 'User'} role` });
    }
  })
  // get all playlists with deleted
  .get('/playlists', async (req, res) => {

    const playlists = await playlistsService.getAllPlaylistsAdmin(playlistsData)();

    res.status(200).json(playlists);
  })
  // ban user by ID
  .post('/ban/:id', loggedUserMiddlware, async (req, res) => {

    const { id } = req.params;

    if (req.user.user_id === +id) {
      return res.status(409).json({ error: 'You cannot ban yourself!' });
    }

    const banData = { ...req.body, user_id: +id };

    const { error, message } = await adminService.banUser(adminData)(banData);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ message: 'User not found!' });
    } else if (error === serviceErrors.DUPLICATE_RECORD) {
      return res.status(409).json({ message: 'User is already banned!' });
    } else {
      res.status(201).json(message);
    }
  })
  // unban user by ID
  .put('/ban/:id', loggedUserMiddlware, async (req, res) => {
    const { id } = req.params;

    const { error } = await adminService.unbanUser(adminData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ message: 'User not found' });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(404).json({ message: 'User is not banned' });
    } else {
      res.json({ message: `User with id ${id} was unbanned!` });
    }
  });

export default adminController;
