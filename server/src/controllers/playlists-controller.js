import express from 'express';
import { authMiddleware } from '../middleware/auth.middleware.js';
import playlistsService from '../services/playlists-service.js';
import playlistsData from '../data/playlists-data.js';
import serviceErrors from '../services/service-errors.js';
import loggedUserMiddleware from '../middleware/logged-user-middleware.js';

const playlistsController = express.Router();

playlistsController
  // get all playlists
  .get('/', async (req, res) => {

    const playlists = await playlistsService.getAllPlaylists(playlistsData)();

    res.status(200).json(playlists);
  })
  // get top 3 playlists
  .get('/topthree', async (req, res) => {

    const topThree = await playlistsService.getTopThreePlaylists(playlistsData)();

    res.status(200).json(topThree);
  })
  .patch('/duration', async (req, res) => {
    const { start_point, end_point } = req.body;

    res.status(200).json(await playlistsService.getDuration(playlistsData)(start_point, end_point));
  })
  // get playlist by id
  .get('/:id', async (req, res) => {
    const { id } = req.params;
    const { error, playlist } = await playlistsService.getPlaylistById(playlistsData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `Playlist with id ${req.params.id} was not found!` });
    }

    res.status(200).json(playlist);
  })
  // get the genres of a playlist
  .get('/:id/genres', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { search } = req.query;
    const { id } = req.params;

    const { error, genres } = await playlistsService.getAllGenresByPlaylistId(playlistsData)(+id, search);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `Playlist with id ${req.params.id} was not found!` });
    } else {
      return res.status(200).json(genres);
    }
  })
  // get the tracks of a playlist
  .get('/:id/tracks', async (req, res) => {
    const { search } = req.query;
    const { id } = req.params;

    const { error, tracks } = await playlistsService.getAllTracksByPlaylistId(playlistsData)(+id, search);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `Playlist with id ${req.params.id} was not found!` });
    } else {
      return res.status(200).json(tracks);
    }
  })
  // generate playlist
  .post('/', authMiddleware, loggedUserMiddleware, async (req, res) => {

    const { error, playlist } = await playlistsService.createPlaylist(playlistsData)(req.body);

    if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).json('Playlist with the same title already exists');
    }

    if (!error) {
      res.status(200).json(playlist);
    }
  })
  // update playlist
  .put('/:id', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id } = req.params;
    const title = req.body.title;
    const user_id = req.user.user_id;
    const role = req.user.role_title;

    const { error, playlist } = await playlistsService.updatePlaylist(playlistsData)(+id, title, user_id, role);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `Playlist with id ${req.params.id} was not found!` });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ msg: 'Operation not allowed!' });
    } else {
      res.status(200).json(playlist);
    }
  })
  // delete playlist
  .delete('/:id', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id } = req.params;
    const user_id = req.user.user_id;
    const role = req.user.role;

    const { error, playlist } = await playlistsService.removePlaylist(playlistsData)(+id, user_id, role);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `Playlist with id ${req.params.id} was not found!` });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ msg: 'Operation not allowed!' });
    } else {
      res.status(200).json(playlist);
    }
  })
  // restore playlist
  .put('/:id/status', authMiddleware, loggedUserMiddleware, async (req, res) => {
    const { id } = req.params;
    const user_id = req.user.user_id;
    const role = req.user.role;

    const { error, playlist } = await playlistsService.restorePlaylist(playlistsData)(+id, user_id, role);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ msg: `Playlist with id ${req.params.id} was not found!` });
    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(403).json({ msg: 'Operation not allowed!' });
    } else {
      res.status(200).json(playlist);
    }
  });

export default playlistsController;
