import express from 'express';
import tracksData from '../data/tracks-data.js';
import tracksService from '../services/tracks-service.js';
// import { authMiddleware } from '../authentication/auth.middleware.js';

const tracksController = express.Router();

// tracksController.use(authMiddleware);

tracksController
  // get all tracks
  .get('/', async (req, res) => {
    const { search, sort, page, limit } = req.query;
    const tracks = await tracksService.getAllTracks(tracksData)(search, sort, page, limit);

    res.status(200).json(tracks);
  });

export default tracksController;
