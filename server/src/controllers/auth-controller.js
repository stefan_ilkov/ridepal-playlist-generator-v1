import express from 'express';
import { authMiddleware } from '../middleware/auth.middleware.js';
import usersService from '../services/users-service.js';
import serviceErrors from '../services/service-errors.js';
import usersData from '../data/users-data.js';
import loggedUserMiddleware from '../middleware/logged-user-middleware.js';


const authController = express.Router();

authController
  // register user
  .post('/register', async (req, res) => {
    const data = req.body;
    const { error, user } = await usersService.createUser(usersData)(data);

    if (error === serviceErrors.DUPLICATE_RECORD) {
      return res.status(409).json({ error: 'Username or email have already been taken!' });
    }

    res.status(201).json({ 'username': user.username, 'email': user.email });
  })
  // login user
  .post('/login', async (req, res) => {
    try {
      const data = req.body;
      const { error, token } = await usersService.loginUser(usersData)(data);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(400).json({ error: 'Username does not exist' });
      }

      if (error === serviceErrors.INVALID_PASSWORD) {
        return res.status(400).json({ error: 'Password does not match username!' });
      }

      res.status(202).json({ token });
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  })
  // logout user
  .post('/logout', authMiddleware, loggedUserMiddleware, async (req, res) => {
    await usersService.logoutUser(usersData)(req.headers.authorization.replace('Bearer ', ''));

    res.json({ message: 'Successfully logged out!' });
  });

export default authController;
