import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

export const getToken = () => localStorage.getItem('token') || '';


const AuthContext = createContext({
  user: null,
  isLoggedIn: false,
  setAuthValue: () => {},
});

export const createSession = (token, auth) => {
  try {
    const user = jwtDecode(token);
    localStorage.setItem('token', token);
    auth.setAuthState({ user, isLoggedIn: true });

    return true;
  } catch (error) {
    console.warn(error);
    return false;
  }
}

export const getUser = () => {
  try {
    return jwtDecode(localStorage.getItem('token') || '');
  } catch (error) {
    return null;
  }
};

export default AuthContext;
