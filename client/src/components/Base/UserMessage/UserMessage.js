/* eslint-disable react/prop-types */
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React from 'react';

const UserMessage = ({ open, handleClose, severity, message }) => {

  return (
    <Snackbar
      autoHideDuration={3000}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      open={open}
      onClose={handleClose}>
      <Alert
        variant="filled"
        onClose={handleClose}
        severity={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default UserMessage;
