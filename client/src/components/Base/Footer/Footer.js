import React from 'react';
import './Footer.css';

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='footer-container'>
        <section className='footer-nav'>
        </section>
       </div>
       <p>Copyright &copy; 2021 All Rights reserved</p>
    </footer>
  );
};

export default Footer;
