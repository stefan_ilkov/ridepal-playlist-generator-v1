import React, { useContext, useState } from 'react';
import './Header.css';
import { Link, useHistory } from 'react-router-dom';
import AuthContext, { getToken } from '../../../providers/AuthContext';
import { BiLogOut } from 'react-icons/bi';
import { CgProfile } from 'react-icons/cg';
import { BASE_URL } from '../../../common/constants';
import UserMessage from '../UserMessage/UserMessage';

const Header = () => {
  const auth = useContext(AuthContext);
  const history = useHistory();

  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: ''
  });
  const userMsgClose = () => {
    setUserMessage({ open: false });
  };


  const logout = () => {

    fetch(`${BASE_URL}/auth/logout`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
      .then(response => response.json())
      .then((data) => {
        setUserMessage({
          open: true,
          severity: "success",
          message: "You have been logged out successfully!"
        });
        setTimeout(() => history.push('/home'), 1000);
      }).catch(error => setUserMessage({
        open: true,
        severity: 'error',
        message: `${error}`
      }));

    localStorage.removeItem('token');

    auth.setAuthValue({
      user: null,
      isLogged: false,
    })
  }

  return (
    <header className='header'>
      <nav>
        <ul>
          <li><Link to='/home'><img className="logo" src='../../../logo-grey.png' alt="logo" /></Link></li>
          <li><Link to='/home'><button className='nav-btn'>HOME</button></Link></li>
          <li><Link to='/playlists'><button className='nav-btn'>PLAYLISTS</button></Link></li>
          {auth.isLoggedIn ? auth.user.role_title === 'Admin' ?
            (
              <>
                <li><Link to='/generation'><button className='nav-btn'>GENERATE PLAYLIST</button></Link></li>
                <li><Link to='/admin'><button className='nav-btn'>ADMIN PANEL</button></Link></li>
              </>
            ) : (
              <>
                <li><Link to='/generation'><button className='nav-btn'>GENERATE PLAYLIST</button></Link></li>
              </>
            ) : ('')}
        </ul>
        <ul>
          {auth.isLoggedIn ? (
            <>
              <p className='logout-text'>Wellcome, {auth.user.username}!</p>
              <button className='logout-btn' onClick={() => {
                logout();
              }}><BiLogOut /></button>
              <Link to='/account' className='profile-btn'><CgProfile /></Link>
            </>
          ) : (
            <li><Link to='/login'><button className='nav-btn'>LOGIN</button></Link></li>
          )}
        </ul>
      </nav>
      <UserMessage
        open={userMessage.open}
        handleClose={userMsgClose}
        severity={userMessage.severity}
        message={<span>{userMessage.message}</span>} />
    </header>
  );
}

export default Header;
