import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './Register.css';
import { useForm } from 'react-hook-form';
import { BASE_URL } from '../../../common/constants.js';
import UserMessage from '../../Base/UserMessage/UserMessage';

const Register = () => {
  const { register, handleSubmit, formState: { errors } } = useForm({
    mode: 'onBlur',
  });

  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: ''
  });
  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const history = useHistory();


  const onSubmit = (data) => {
    fetch(`${BASE_URL}/auth/register`, {
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json',
      }
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.error);
        }
        setUserMessage({
          open: true,
          severity: "success",
          message: "You have been registered successfully!"
        });
        setTimeout(() => {
          history.push('/login')
        }, 1500);
      })
      .catch(error => setUserMessage({
        open: true,
        severity: 'error',
        message: `${error}`
      }));;
  }

  return (
    <div className='register-box'>
      <div className='register-form'>
        <h1 className='register-title'>Sign up</h1>
        <div className='register-inputs'>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className='input-div'>
              <input type='text' placeholder='First Name' name='first_name' {...register('first_name', { required: "Required", })} />
              {errors.first_name && <p className='error'>Required field!</p>}
            </div>
            <div className='input-div'>
              <input type='text' placeholder='Last Name' name='last_name' {...register('last_name', { required: "Required", })} />
              {errors.last_name && <p className='error'>Required field!</p>}
            </div>
            <div className='input-div'>
              <input type='text' placeholder='Username' name='username' {...register('username', { required: "Required", minLength: 3 })} />
              {errors.username && <p className='error'>Incorrect username!</p>}
            </div>
            <div className='input-div'>
              <input type='text' placeholder='Email' name='email' {...register('email', { required: "Required", pattern: /^[^\s@,]+@[^\s@,]+\.[^\s@,]+$/ })} />
              {errors.email && <p className='error'>Invalid email!</p>}
            </div>
            <div className='input-div'>
              <input type='password' placeholder='Password' name='password' {...register('password', { required: "Required", minLength: 6 })} />
              {errors.password && <p className='error'>Minimum length of 6!</p>}
            </div>
            <button className='submit-btn' type='submit'>Submit</button>
          </form>
        </div>
        <UserMessage
          open={userMessage.open}
          handleClose={userMsgClose}
          severity={userMessage.severity}
          message={<span>{userMessage.message}</span>} />
      </div>
    </div>
  )
}

export default Register;
