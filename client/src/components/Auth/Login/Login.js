import React, { useState, useContext } from 'react';
import './Login.css';
import { useForm } from 'react-hook-form';
import { Link, useHistory } from 'react-router-dom';
import { BASE_URL } from '../../../common/constants';
import jwtDecode from 'jwt-decode';
import AuthContext from '../../../providers/AuthContext';
import UserMessage from '../../Base/UserMessage/UserMessage';

const Login = () => {
  const { register, handleSubmit, formState: { errors } } = useForm({
    mode: 'onBlur',
  });

  const auth = useContext(AuthContext);
  const history = useHistory();

  const [userMessage, setUserMessage] = useState({
    open: false,
    message: '',
    severity: ''
  });
  const userMsgClose = () => {
    setUserMessage({ open: false });
  };

  const onSubmit = (data) => {

    fetch(`${BASE_URL}/auth/login`, {
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json',
      }
    })
      .then(response => response.json())
      .then((data) => {
        if (data.token) {
          setTimeout(() => {
            const user = jwtDecode(data.token);
            localStorage.setItem('token', data.token);
            auth.setAuthValue({ user, isLoggedIn: true });
            history.push('/home')
          }, 1500);
          setUserMessage({
            open: true,
            severity: "success",
            message: "You have been logged in successfully!"
          });
        } else {
          throw new Error(data.error);
        }
      }).catch(error => setUserMessage({
        open: true,
        severity: 'error',
        message: `${error}`
      }));;
  }

  return (
    <div className='login-box'>
      <div className='login-form'>
        <h1 className='login-title'>Login</h1>
        <div className='login-inputs'>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className='login-input'>
              <input type='text' placeholder='Username' name='username' {...register('username', { required: 'Required', minLength: 3 })} />
              {errors.username && <p className='error'>Incorrect username!</p>}
            </div>
            <div className='login-input'>
              <input type='password' placeholder='Password' name='password' {...register('password', { required: 'Required', minLength: 6 })} />
              {errors.password && <p className='error'>Incorrect password!</p>}
            </div>
            <p>You don't have an accout?<Link className='signup-link' to='/signup'>Signup</Link></p>
            <button className='submit-btn' type='submit'>Submit</button>
          </form>
        </div>
        <UserMessage
          open={userMessage.open}
          handleClose={userMsgClose}
          severity={userMessage.severity}
          message={<span>{userMessage.message}</span>} />
      </div>
    </div>
  )
}

export default Login;
