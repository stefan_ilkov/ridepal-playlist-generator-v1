import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './Tracks.css';
import Loading from '../Pages/Loading/Loading';
import AppError from '../Pages/AppError/AppError';
import { BASE_URL } from '../../common/constants';

const Tracks = ({ playlist }) => {
  const [tracks, setTracks] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/playlists/${playlist.id}/tracks`)
      .then((response) => response.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setTracks(data);
        } else {
          throw new Error(data.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);


  if (loading) {
    return (
      <Loading>
        <h1>Loading tracks...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <ul className='tracks'>
      {tracks.map((track) => {
        return (
          <li key={track.id} className='track'>
            <div className='artist-pic'>
              <img src={track.pic} alt='artist'></img>
            </div>
            <div className='track-song'>
              <p><b>{track.title}</b></p>
              <p>{track.artist}</p>
            </div>
            <div className='track-genre'>
              <p><b>Genre: </b>{track.genre}</p>
              <p><b>Rank: </b>{track.rank}</p>
            </div>
            <div className='track-album'>
              <p><b>Album: </b>{track.album}</p>
              <p><b>Release: </b>{new Date(track.release_date).toLocaleDateString("en-US")}</p>
            </div>
            <p className='playlist-duration'><b>{track.duration}</b></p>
            <audio controls className='audio'>
              <source src={track.preview_url} type='audio/mp3' />
            </audio>
          </li>
        )
      })}
    </ul>
  )
}

Tracks.propTypes = {
  playlist: PropTypes.object.isRequired
};

export default Tracks;
