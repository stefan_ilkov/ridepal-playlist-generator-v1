import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import Loading from '../../Pages/Loading/Loading';
import './AdminPage.css';
import { makeStyles } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TabPanel from '@material-ui/lab/TabPanel';
import TabList from '@material-ui/lab/TabList';
import TabContext from '@material-ui/lab/TabContext';
import AdminList from './AdminList';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    paper: {
        marginRight: theme.spacing(2),
    },
    title: {
        margin: "1%",
        textAlign: "center",
        fontWeight: "bold",
    }
}));

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <>
            <Tab
                component="a"
                onClick={(event) => {
                    event.preventDefault();
                }}
                {...props}
            >
            </Tab>
        </>
    );
}

const AdminPage = () => {
    const classes = useStyles();
    const [value, setValue] = useState('1');
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setLoading(false)
    }, []);

    return (
        <main className='admin-main'>
            <Grid >
                {loading
                    ? <Grid item xs={12} align="center">< Loading /></Grid > :
                    <>
                        <Typography variant="h4" className={classes.title}>
                            Admin Panel
                        </Typography>
                        <TabContext value={value}>
                            <TabList
                                variant="fullWidth"
                                value={value}
                                onChange={handleChange}
                                aria-label="nav"
                                component="nav"
                            >
                                <LinkTab label="Playlists" value="1" {...a11yProps(0)} />
                                <LinkTab label="Users" value="2" {...a11yProps(1)} />
                            </TabList>
                            <TabPanel align="center" value="1"><AdminList url={'playlists'} /></TabPanel>
                            <TabPanel align="center" value="2"><AdminList url={'users'} /></TabPanel>
                        </TabContext>
                    </>}
            </Grid>
        </main>
    );
};

export default AdminPage;
