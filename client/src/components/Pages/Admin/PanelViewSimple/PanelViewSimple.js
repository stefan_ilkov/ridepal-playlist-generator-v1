/* eslint-disable react/prop-types */
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React, { useState } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import DeleteDialog from '../DeleteDialog/DeleteDialog';
import EditDialog from '../EditDialog/EditDialog';
import { makeStyles } from '@material-ui/core/styles';
import UserMessage from '../../../Base/UserMessage/UserMessage';
import QueueMusicIcon from '@material-ui/icons/QueueMusic';

const useStyles = makeStyles((theme) => ({
    DeletedPanel: {
        backgroundColor: 'Salmon'
    },
}));


const PanelViewSimple = ({ item, loadItems }) => {
    const classes = useStyles();
    const [openDelete, setOpenDelete] = useState(false);
    const [openEditBook, setOpenEditBook] = useState(false);

    const [userMessage, setUserMessage] = useState({
        open: false,
        message: '',
        severity: ''
    });

    const userMsgClose = () => {
        setUserMessage({ open: false });
    };

    const handleDeleteOpen = () => {
        setOpenDelete(true);
    };


    const handleEditOpen = () => {
        setOpenEditBook(true);
    };


    const handleDeleteClose = () => {
        loadItems();
        setOpenDelete(false);
    };


    const handleEditClose = () => {
        loadItems();
        setOpenEditBook(false);
    };

    return (
        <>
            <ListItem className={item.is_deleted ? classes.DeletedPanel : ''} divider={true}>
                <ListItemAvatar>
                    <Avatar>
                        {item.title ? <QueueMusicIcon /> : <PersonIcon />}
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={(item.title ? item.title : item.username ? item.username
                        : `${item.content.slice(0, 40)}${item.content.length > 40 ? '...' : ''}`)
                        + (item.is_deleted ? ' (deleted)' : '')}
                />
                <ListItemSecondaryAction>
                    {!item.role_id ?
                        <IconButton onClick={handleEditOpen} edge="end" aria-label="ban">
                            <EditIcon />
                        </IconButton> : ''}
                    <IconButton onClick={handleDeleteOpen} edge="end" aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                    <DeleteDialog open={openDelete} handleClose={handleDeleteClose} item={item} setUserMessage={setUserMessage} />
                    <EditDialog open={openEditBook} handleClose={handleEditClose} item={item} setUserMessage={setUserMessage} />
                </ListItemSecondaryAction>
            </ListItem>
            <UserMessage
                open={userMessage.open}
                handleClose={userMsgClose}
                severity={userMessage.severity}
                message={<span>{userMessage.message}</span>} />
        </>
    );
};

export default PanelViewSimple;
