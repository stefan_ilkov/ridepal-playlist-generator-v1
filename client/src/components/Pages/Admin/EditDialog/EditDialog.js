/* eslint-disable react/prop-types */
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';

const EditDialog = ({ open, handleClose, item, setUserMessage }) => {

    const [title, updateTitle] = useState(item.title);

    const editPlaylist = () => {
        fetch(`${BASE_URL}/playlists/${item.id}`, {
            method: 'PUT',
            headers: { 'Authorization': 'Bearer ' + getToken(), 'Content-Type': 'application/json' },
            body: JSON.stringify({ title }),
        })
            .then(res => res.json())
            .then(data => {
                if (!data.error) {
                    setUserMessage({
                        open: true,
                        severity: "success",
                        message: "Playlist title updated successfully!"
                    });
                } else {
                    throw new Error(data.error);
                };
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => handleClose());

    };


    const editUser = () => {
        fetch(`${BASE_URL}/admin/user/${item.id}`, {
            method: 'PUT',
            headers: { 'Authorization': 'Bearer ' + getToken(), 'Content-Type': 'application/json' },
            body: JSON.stringify({ role_id: item.role_title === 'Admin' ? 2 : 1 }),
        })
            .then(res => res.json())
            .then(data => {
                if (!data.error) {
                    setUserMessage({
                        open: true,
                        severity: "success",
                        message: "User role updated successfully!"
                    });
                } else {
                    throw new Error(data.error);
                };
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => handleClose());

    };

    const handleEdit = () => {
        item.genres ? editPlaylist() : editUser();
    };

    return (
        <>
            { item.genres ?
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Edit playlist title</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            <TextField
                                autoFocus
                                margin="dense"
                                id="Playlist Title"
                                label="Playlist Title"
                                type="Playlist Title"
                                fullWidth
                                defaultValue={title}
                                onChange={(e) => updateTitle(e.target.value)}
                            />
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleEdit} color="secondary" >
                            Update Playlist Title
                </Button>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                </Button>
                    </DialogActions>
                </Dialog>
                :
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Edit user role</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            Change user role to {item.role_title === 'Admin' ? 'Admin' : 'User'}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleEdit} color="secondary" >
                            Change user role
                        </Button>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            }
        </>
    );
};

export default EditDialog;
