/* eslint-disable react/prop-types */
import { Grid, Typography } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import Loading from '../../Pages/Loading/Loading';
import PanelViewSimple from './PanelViewSimple/PanelViewSimple';
import List from '@material-ui/core/List';
import { BASE_URL } from '../../../common/constants';
import { getToken } from '../../../providers/AuthContext';


const AdminList = ({ url }) => {
    const [items, updateItems] = useState([]);
    const [loading, setLoading] = useState(true);

    const [userMessage, setUserMessage] = useState({
        open: false,
        message: '',
        severity: ''
    });

    const loadItems = () => {
        fetch(`${BASE_URL}/${url === 'users' ? 'users' : 'admin/playlists'}`, {
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
            .then(res => res.json())
            .then(data => {
                if (Array.isArray(data)) {
                    updateItems([...data]);
                } else {
                    throw new Error(data.error);
                }
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => setLoading(false));
    }

    useEffect(() => {
        loadItems();
    }, []);


    return (
        <>
            {loading
                ? <Grid item xs={12} align="center" >< Loading /></Grid >
                : items.length === 0
                    ? <Grid item xs={12}>
                        <Typography variant="h5" align="center">No items to show...</Typography>
                    </Grid >
                    : <List>
                        {items.map(item =>
                            <Grid item xs={8} key={item.id} >
                                <PanelViewSimple item={item} loadItems={loadItems} />
                            </Grid>
                        )}
                    </List>
            }
        </>
    );
};

export default AdminList;
