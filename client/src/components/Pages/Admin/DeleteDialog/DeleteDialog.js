/* eslint-disable react/prop-types */
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import React from 'react';
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';

const DeleteDialog = ({ open, handleClose, item, setUserMessage }) => {

    const deleteUser = () => {
        fetch(`${BASE_URL}/users/${item.id}`, {
            method: 'DELETE',
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
            .then(res => res.json())
            .then(data => {
                if (!data.error) {
                    setUserMessage({
                        open: true,
                        severity: "success",
                        message: "User deleted successfully!"
                    });
                } else {
                    throw new Error(data.error);
                };
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => handleClose());
    };

    const deletePlaylist = () => {
        fetch(`${BASE_URL}/playlists/${item.id}`, {
            method: 'DELETE',
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
            .then(res => res.json())
            .then(data => {
                if (!data.error) {
                    setUserMessage({
                        open: true,
                        severity: "success",
                        message: "Playlist deleted successfully!"
                    });
                } else {
                    throw new Error(data.error);
                };
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => handleClose());
    };

    const restoreUser = () => {
        fetch(`${BASE_URL}/users/${item.id}`, {
            method: 'PUT',
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
            .then(res => res.json())
            .then(data => {
                if (!data.error) {
                    setUserMessage({
                        open: true,
                        severity: "success",
                        message: "User restored successfully!"
                    });
                } else {
                    throw new Error(data.error);
                };
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => handleClose());
    };

    const restorePlaylist = () => {
        fetch(`${BASE_URL}/playlists/${item.id}/status`, {
            method: 'PUT',
            headers: { 'Authorization': 'Bearer ' + getToken() }
        })
            .then(res => res.json())
            .then(data => {
                if (!data.error) {
                    setUserMessage({
                        open: true,
                        severity: "success",
                        message: "Playlist restored successfully!"
                    });
                } else {
                    throw new Error(data.error);
                };
            })
            .catch(error => setUserMessage({
                open: true,
                severity: 'error',
                message: `${error}`
            }))
            .finally(() => handleClose());
    };

    const handleDelete = () => {

        if (item.email && item.is_deleted === 1) {
            restoreUser();
        }

        if (item.email && item.is_deleted === 0) {
            deleteUser();
        }

        if (item.title && item.is_deleted === 0) {
            deletePlaylist();
        }

        if (item.title && item.is_deleted === 1) {
            restorePlaylist();
        }

    };

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Delete item</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                    {item.is_deleted ? 'Do you wish to undo the deletion of this item in the database?'
                        : 'Are you certain you want to delete this item?'}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDelete} color="secondary" >
                    {item.is_deleted ?
                        'UNDELETE'
                        : 'DELETE'}
                </Button>
                <Button onClick={handleClose} color="primary">
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default DeleteDialog;
