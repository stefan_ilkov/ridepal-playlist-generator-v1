import React from 'react';
import './NotFound.css';

const NotFound = () => {
    return (
        <main className='not-found' style={{backgroundImage: `url(${process.env.PUBLIC_URL}/white-and-blue.jpg`}}>
             <h1>Ups, something went wrong... </h1>
        </main>
    );
}
  
export default NotFound;
