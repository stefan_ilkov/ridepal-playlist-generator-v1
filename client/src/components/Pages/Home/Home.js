import React, { useContext } from 'react';
import TopThreePlaylists from '../../Playlists/TopThreePlaylists/TopThreePlaylists.js';
import './Home.css';
import { Link } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';

const Home = () => {


  const auth = useContext(AuthContext);
  return (
    <main className='main'>
      <section className='main-img' style={{ backgroundImage: `url(/headphones-neon.png` }}>
        <h1><span>RIDEPAL PLAYLIST</span> GENERATOR</h1>
        <p>Pick your favorite music and generate a playlists for specific travel duration periods</p>
        <p>Keep your playlist in your profile</p>
        <p>Browse playlists created by other users</p>
      </section>
      <section className='main-how-work' >
        <h3>HOW IT WORKS</h3>
        <ul>
          <li>
            <p className='number'>1</p>
            <h4>CHOOSE YOUR DESTINATION</h4>
            <p>
              Pick your start and your end point of your travel destination
            </p>
          </li>
          <li>
            <p className='number'>2</p>
            <h4>CHOOSE YOUR FAVORITE MUSIC</h4>
            <p>
              Choose your favorite music based on your favorite genre
            </p>
          </li>
          <li>
            <p className='number'>3</p>
            <h4>ENJOY</h4>
            <p>
              Enjoy listening to your favorite music while traveling and browse through the playlists created by others
            </p>
          </li>
        </ul>
      </section>
      <section className='main-top-playlists'>
        <h1>TOP 3 PLAYLISTS</h1>
        <TopThreePlaylists />
      </section>
      <section className='main-bottom'>
        <h1>Are you ready for an adventure?</h1>
        {auth.isLoggedIn ?
          <>
            <p>Go for a ride</p>
            <Link to='/generation' className='signup-btn'>Create your own playlist</Link>
          </> :
          <>
            <p>Join our family now:</p>
            <Link to='/signup' className='signup-btn'>Signup</Link>
          </>}
      </section>
    </main>
  );
}

export default Home;
