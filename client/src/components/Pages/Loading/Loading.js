import React from 'react';
import './Loading.css';

const Loading = () => {
  return (
    <main className='loading'>
      <h2 className='loading-h4'>Loading...</h2>
    </main>
  )
};

export default Loading;
