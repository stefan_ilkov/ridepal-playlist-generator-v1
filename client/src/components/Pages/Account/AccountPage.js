import React from 'react';
import { makeStyles, Grid } from '@material-ui/core';
import { deepOrange } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import MyPlaylists from '../../Playlists/AllPlaylists/MyPlaylists';
import Profile from './Profile';

const useStyles = makeStyles({
  gridContainer: {
    padding: '15px',
    margin: 0,
    width: '100%'
  },
  titleContainer: {
    padding: '15px',
    marginLeft: '5em',
    width: '100%',
    alignItems: 'center'
  },
  avatar: {
    width: '2em',
    padding: '15px',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: deepOrange[500],
  },
});

const AccountPage = () => {
  const classes = useStyles();

  return (
    <main className='main-all-playlists'>
      <Grid container spacing={2} direction="row" className={classes.gridContainer}>
        <Profile />
        <Grid item xs={10} container spacing={5} className={classes.gridContainer}>
          <Grid item xs={10} container spacing={2} className={classes.titleContainer} justify="center">
            <Typography variant="h4" component="h3" >
              My Playlists
            </Typography>
          </Grid>
          <MyPlaylists />
        </Grid>
      </Grid>
    </main >
  )
}

export default AccountPage;
