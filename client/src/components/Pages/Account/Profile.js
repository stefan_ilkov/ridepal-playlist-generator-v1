/* eslint-disable react/prop-types */
import React, { useContext, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import { Box, makeStyles, Grid } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import PersonIcon from '@material-ui/icons/Person';
import Divider from '@material-ui/core/Divider';
import AuthContext from '../../../providers/AuthContext.js';
import { deepOrange } from '@material-ui/core/colors';
import Loading from '../../Pages/Loading/Loading.js';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

const useStyles = makeStyles({
  gridContainer: {
    padding: '15px',
    margin: 0,
    width: '100%'
  },
  avatar: {
    width: '2em',
    padding: '10px',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: deepOrange[500],
  },
});

const Profile = () => {

  const classes = useStyles();
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);


  if (loading) {
    if (loading) {
      return <Grid item xs={2}><Loading /></Grid>;
    }
  }

  return (
    <Grid item xs={2} spacing={3} className={classes.gridContainer}>
      <Box>
        <Avatar className={classes.avatar} >{user.username[0].toUpperCase()}</Avatar>
        <br />
        <div >
          <List>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <PersonIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Username"
                secondary={user.username}
              />
            </ListItem>
            <Divider variant="inset" component="li" />
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <ContactMailIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Email"
                secondary={user.email}
              />
            </ListItem>
            <Divider variant="inset" component="li" />
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <VerifiedUserIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Role Title"
                secondary={user.role_title}
              />
            </ListItem>
            <Divider variant="inset" component="li" />
          </List>
        </div>
      </Box>
    </Grid>
  );
};

export default Profile;
