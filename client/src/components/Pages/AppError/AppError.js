import React from 'react';
import PropTypes from 'prop-types';
import './AppError.css';

const AppError = ({ message }) => {

  return (
    <main className='app-error' style={{backgroundImage: `url(${process.env.PUBLIC_URL}/white-and-blue.jpg`}}>
      <h1>{message}</h1>
    </main>
  )
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
