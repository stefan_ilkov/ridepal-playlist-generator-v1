import React, { useState, useEffect, useContext } from 'react';
import './Playlist.css';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Loading from '../../Pages/Loading/Loading';
import AppError from '../../Pages/AppError/AppError';
import { BASE_URL } from '../../../common/constants';
import Tracks from '../../Tracks/Tracks';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AuthContext from '../../../providers/AuthContext';
import EditPlaylist from '../EditPlaylist';
import DeletePlaylist from '../DeletePlaylist';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    boxShadow: '0 0 1em 0 rgb(16, 42, 143)'
  },
  heading: {
    fontSize: theme.typography.h6.fontSize,
    fontWeight: theme.typography.fontWeightBold,
    color: 'rgb(16, 42, 143)'
  },
}));

const Playlist = ({ history, match }) => {
  const classes = useStyles();

  const [playlist, setPlaylist] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [tracks, setTracks] = useState([]);

  const auth = useContext(AuthContext);

  const { id } = match.params;
  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/playlists/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setPlaylist(data);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);

  if (loading) {
    return (
      <Loading>
        <h1>Loading playlist...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <main className='main-playlist'>
      <div className='single-playlist'>
        <ul className='playlist-cover'>
          <li className='img-box'><img src={playlist.pic} alt='img' className='img-playlist'></img></li>
        </ul>
        <ul className='playlist-details'>
          <li className='playlist-header'>
            <h1>{playlist.title}</h1>
            {auth.isLoggedIn && ((auth.user.user_id === playlist.user_id) || (auth.user.role_title === 'Admin')) ? (
              <div className='playlist-btns'>
                <EditPlaylist id={playlist.id} />
                <DeletePlaylist id={playlist.id} />
              </div>
            ) : ('')}
          </li>
          <li><p><b>Average rank:  </b> {playlist.rank}</p></li>
          <li><p><b>Duration:  </b> {Math.floor((playlist.duration / 60))} min</p></li>
          {auth.isLoggedIn ? (
            <>
              <li><p><b>Genres:  </b> {playlist.genres}</p></li>
              <li><p><b>Start point:  </b> {playlist.start_point}</p></li>
              <li><p><b>End point:  </b> {playlist.end_point}</p></li>
              <li><p><b>Created by:  </b> {playlist.username}</p></li>
            </>
          ) : (
            <li className='playlist-info'>
              <p>In order to see the whole info please <Link to='/login' className='info-login'>Login</Link>.</p>
              <p>If you don't have an account, you can <Link to='/signup' className='info-signup'>Signup</Link>.</p>
            </li>
          )}
        </ul>
      </div>
      <div className='accordion'>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls='panel1a-content'
            id='panel1a-header'
          >
            <Typography className={classes.heading}>Checkout the tracks:</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              <Tracks playlist={playlist} />
            </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
    </main>
  )
}

Playlist.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default Playlist;
