/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import './GeneratePlaylist.css';
import { useForm } from 'react-hook-form';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
}));


const ActiveStepper = ({ activeStep, getSteps }) => {

  const classes = useStyles();
  const steps = getSteps();

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </div >
  )
}

export default ActiveStepper;
