/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import './GeneratePlaylist.css';
import Districts from './Districts.js';


const FirstForm = ({ open, register }) => {

  return (
    <>
      < div className='input-div district' >
        <label htmlFor='start-district' className='district-label'>Choose start point district</label>
        <select
          name='start-district'
          {...register('start-district', { required: 'Required', })}
        >
          <Districts />
        </select>
      </div >
      <div className='input-div'>
        <input
          type='text'
          name='start-city'
          placeholder='Enter start point city'
          {...register('start-city', { required: 'Required', })}
        />
      </div>
      <div className='input-div'>
        <input
          type='text'
          name='start-street'
          placeholder='Enter start point street'
          {...register('start-street')}
        />
      </div>
      <div className='input-div num'>
        <label>Enter street number</label>
        <input
          type='number'
          name='start-number'
          min='1'
          max='100'
          {...register('start-number')}
        />
      </div>
      <div className='input-div district'>
        <label htmlFor='end-district' className='district-label'>Choose end point district</label>
        <select
          name='end-district'
          {...register('end-district', { required: 'Required', })}
        >
          <Districts />
        </select>
      </div>
      <div className='input-div'>
        <input
          type='text'
          name='end-city'
          placeholder='Enter end point city'
          {...register('end-city', { required: 'Required', })}
        />
      </div>
      <div className='input-div'>
        <input
          type='text'
          name='end-street'
          placeholder='Enter end point street'
          {...register('end-street')}
        />
      </div>
      <div className='input-div num'>
        <label>Enter street number</label>
        <input
          type='number'
          name='end-number'
          min='1'
          max='100'
          {...register('end-number')}
        />
      </div>
    </>
  )
}

export default FirstForm;
