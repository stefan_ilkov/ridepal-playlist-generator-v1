/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import './GeneratePlaylist.css';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    width: 500,
    display: 'inline-block',
  },
});

const FinalForm = ({ register, values, setValues, image, genres }) => {
  const [value, setValue] = useState(0);
  const classes = useStyles();

  const totalGenreValues = genres.reduce((acc, element) => {
    return acc + element[1]
  }, 0);

  return (
    <>
      <div className='genres'>
        <div className={classes.root}>
          <div className='location'>
            <p><b>Start Location:</b></p>
            <div><b>District: </b>{values['start-district']}</div>
            <div><b>City: </b>{values['start-city']}</div>
            <div><b>Street: </b>{values['start-street']}</div>
            <div><b>Number: </b>{values['start-number']}</div>
          </div>
          <div className='location'>
            <p><b>End Location:</b></p>
            <div><b>District: </b>{values['end-district']}</div>
            <div><b>City: </b>{values['end-city']}</div>
            <div><b>Street: </b>{values['end-street']}</div>
            <div><b>Number: </b>{values['end-number']}</div>
          </div>
          <div className='repeat-artist'>
            <p><b>Repeating artists:</b>
              {values.checkbox ? <span>Yes</span> : <span>No</span>}</p>
          </div>
          <div className='final-pic'>
            {image ? <img src={image} alt='playlist' /> : 'No image selected'}
          </div>

          <div>
            <p><b>Genres:</b></p>
            {genres.map(genre => {
              return (
                <div className='last'>
                  <p><b>Genre Name: </b>{genre[0]}</p>
                  <p><b>Genre Percentage: </b>{Math.round((genre[1] / totalGenreValues) * 100)}%</p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  )
}

export default FinalForm;
