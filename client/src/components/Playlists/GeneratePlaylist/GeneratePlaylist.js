import React, { useState, useContext } from 'react';
import './GeneratePlaylist.css';
import { useForm } from 'react-hook-form';
import { makeStyles } from '@material-ui/core/styles';
import FirstForm from './FirstForm.js';
import SecondForm from './SecondForm.js';
import ActiveStepper from './Stepper.js';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ThirdForm from './ThirdForm.js';
import FinalForm from './FinalForm.js';
import { BASE_URL } from '../../../common/constants';
import AuthContext, { getToken } from '../../../providers/AuthContext';
import { genreIds } from '../../../common/genres-enum';
import { useHistory } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return 'Select path journey...';
    case 1:
      return 'Choose from our special selection';
    case 2:
      return 'Customize as you like';
    case 3:
      return 'Generate';
    default:
      return 'Unknown stepIndex';
  }
}

function getSteps() {
  return ['Select starting point and end point for your journey', 'Reccomendations', 'Customize Playlist', 'Generate Playlist'];
}

const GeneratePlaylist = () => {

  let history = useHistory();
  const classes = useStyles();
  const { user } = useContext(AuthContext);

  const [activeStep, setActiveStep] = useState(0);

  const { register, handleSubmit } = useForm();
  const [values, setValues] = useState([]);
  const [image, setImage] = useState('');
  const [genres, setGenres] = useState([]);
  const [duration, setDuration] = useState(0);

  const onSubmit = (currentValues) => {
    setValues(currentValues);
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleGenerate = () => {
    const sum = genres.reduce((acc, element) => {
      return acc + element[1]
    }, 0);

    const body = {
      'start_point': {
        "adminDistrict": values['start-district'],
        "locality": values['start-city'],
        "postalcode": "-",
        "addressLine": values['start-street'],
        "number": values['start-number']
      },
      'end_point': {
        "adminDistrict": values['end-district'],
        "locality": values['end-city'],
        "postalcode": "-",
        "addressLine": values['end-street'],
        "number": values['end-number']
      },
      'duration': duration,
      'user_id': user.user_id,
      'title': values['title'],
      'repeat_artists': values['checkbox'],
      'image': image,
      'genres': genres.map(genre => {
        return { 'genre_id': genreIds[genre[0].toUpperCase()], 'percentage': Math.round((genre[1] / sum) * 100) }
      })
    }
    fetch(`${BASE_URL}/playlists`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(body),
    }).then(data => data.json())
      .then(data => {
        if (data.error) {
          throw new Error(data.error);
        } else {
          history.push(`/playlists/${data.id}`);
        }
      })
      .catch((error) => error.message);
    setActiveStep(0);
  };

  const steps = getSteps();

  return (
    <div className='create-pl-box'>
      <div className='create-pl-form'>
        <h1 className='create-title'>Generate playlist</h1>
        <div className='create-pl-inputs'>
          <form onSubmit={handleSubmit(onSubmit)}>
            {activeStep === 0 ?
              <FirstForm register={register}></FirstForm>
              : activeStep === 1 ?
                <SecondForm register={register} values={values} duration={duration} setDuration={setDuration}></SecondForm>
                : activeStep === 2 ?
                  <ThirdForm register={register} values={values} setImage={setImage} genreValues={genres} setGenres={setGenres} ></ThirdForm>
                  : activeStep === 3 ?
                    <FinalForm register={register} values={values} image={image} genres={genres}></FinalForm>
                    : ''}
            <div>
              {activeStep !== steps.length - 1 ?
                <div>
                  <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                  <div>
                    <Button
                      variant="contained"
                      color="primary"
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.backButton}
                    >
                      Back
                    </Button>
                    <Button variant="contained" color="primary" type='submit'>Next</Button>
                  </div>
                </div>
                : <div>
                  <Typography className={classes.instructions}>All steps completed</Typography>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.backButton}
                  >
                    Back
                  </Button>
                  <Button variant="contained" color="primary" onClick={handleGenerate}>Generate</Button>
                </div>
              }
            </div >
          </form>
          <ActiveStepper activeStep={activeStep} getSteps={getSteps} />
        </div>
      </div>
    </div>
  )
}

export default GeneratePlaylist;
