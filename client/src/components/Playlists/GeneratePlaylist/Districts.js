import React from 'react'

const Districts = () => {
  return (
    <>
      <option value='blagoevgrad'>Blagoevgrad</option>
      <option value='burgas'>Burgas</option>
      <option value='dobrich'>Dobrich</option>
      <option value='gabrovo'>Gabrovo</option>
      <option value='haskovo'>Haskovo</option>
      <option value='kardzhali'>Kardzhali</option>
      <option value='kyustendil'>Kyustendil</option>
      <option value='lovech'>Lovech</option>
      <option value='montana'>Montana</option>
      <option value='pazardzhik'>Pazardzhik</option>
      <option value='pernik'>Pernik</option>
      <option value='pleven'>Pleven</option>
      <option value='plovdiv'>Plovdiv</option>
      <option value='razgrad'>Razgrad</option>
      <option value='ruse'>Ruse</option>
      <option value='shumen'>Shumen</option>
      <option value='silistra'>Silistra</option>
      <option value='sliven'>Sliven</option>
      <option value='smolyan'>Smolyan</option>
      <option value='sofia-city'>Sofia City</option>
      <option value='sofia'>Sofia</option>
      <option value='stara-zagora'>Stara Zagora</option>
      <option value='targovishte'>Targovishte</option>
      <option value='varna'>Varna</option>
      <option value='veliko-tarnovo'>Veliko Tarnovo</option>
      <option value='vidin'>Vidin</option>
      <option value='vratsa'>Vratsa</option>
      <option value='yambol'>Yambol</option>
    </>
  )
}

export default Districts;
