/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import './GeneratePlaylist.css';
import { BASE_URL } from '../../../common/constants.js';
import Loading from '../../Pages/Loading/Loading';
import AppError from '../../Pages/AppError/AppError';
import { Link } from 'react-router-dom';

const SecondForm = ({ register, values, duration, setDuration }) => {

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [reccomendedPlaylists, setReccomendedPlaylists] = useState([]);

  useEffect(() => {
    setLoading(true);
    const body = {
      start_point: {
        "adminDistrict": values["start-district"],
        "locality": values["start-city"],
        "postalcode": "-",
        "addressLine": values["start-street"],
        "number": values["start-number"]
      },
      end_point: {
        "adminDistrict": values["end-district"],
        "locality": values["end-city"],
        "postalcode": "-",
        "addressLine": values["end-street"],
        "number": values["end-number"]
      }
    };
    fetch(`${BASE_URL}/playlists/duration`, {
      method: 'PATCH',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((data) => {
        const { duration, reccomendedPlaylists } = data;
        setDuration(duration);
        setReccomendedPlaylists(reccomendedPlaylists);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);


  if (loading) {
    return (
      <Loading>
        <h1>Loading playlists...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <>
      <p>Calculated travel Duration: {duration} minutes</p>
      <h3 className='generate-h3'>Choose from our recomended playlists</h3>
      { reccomendedPlaylists.length === 0 ?
        <p>No playlists with this duration found in our database</p>
        : reccomendedPlaylists.map(playlist => {
          return (
            <div key={playlist.id} className='playlist recomended'>
              <Link to={`/playlists/${playlist.id}`}>
                <img src={playlist.pic} alt='img' className='img-playlist'></img>
              </Link>
              <p><b>Playlist title: </b> {playlist.title}</p>
              <p><b>Duration: </b>{Math.floor((playlist.duration/60))} min</p>
              <p><b>Average rank: </b> {playlist.rank}</p>
              <p><b>Number of tracks: </b> {playlist.tracks}</p>
            </div>
          )
        })}
      <h3 className='generate-h3'>Or create your own:</h3>
      <div className='create-name'>
      <label>Name it: </label>
        <input
          className=''
          type='text'
          name='title'
          placeholder='Enter playlist title'
          {...register('title', { required: 'Required', })}
        />
      </div>
      <div className='checkbox-div'>
        <input
          type='checkbox'
          name='checkbox'
          placeholder='Enter genre percentage'
          {...register('checkbox')}
        />
        <label>Select repeat artist</label>
      </div>
    </>
  )
}

export default SecondForm;
