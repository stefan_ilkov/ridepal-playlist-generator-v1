/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import './GeneratePlaylist.css';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Loading from '../../Pages/Loading/Loading';
import AppError from '../../Pages/AppError/AppError';

const useStyles = makeStyles({
  root: {
    width: 500,
    marginLeft: '5em',
  },
});

const getGenres = () => {
  return ['Rock', 'Rap', 'Pop', 'Jazz',];
}

const ThirdForm = ({ register, values, setValues, setImage, setGenres, genreValues }) => {

  const genres = getGenres();

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const classes = useStyles();
  const [pictureURLs, setPicturesURLs] = useState([]);

  useEffect(() => {
    setLoading(true);
    fetch(`https://pixabay.com/api/?key=21887371-88640ab63c013864782c5ed0f&orientation=horizontal&min_height=3000&min_width=5000&q=music+instrument&per_page=200`)
      .then((response) => response.json())
      .then((data) => {
        const pictures = Array.from({ length: 6 }).map(picture => {
          const item = data.hits.splice(Math.floor(Math.random() * data.hits.length), 1);
          return item[0].webformatURL;
        });
        setPicturesURLs(pictures);
        setImage(pictures[Math.floor(Math.random() * pictures.length)])
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);


  if (loading) {
    return (
      <Loading>
        <h1>Loading playlists...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  const handleImage = (img) => {
    setImage(img);
  }

  const handleGenres = (e, genre, value) => {
    const index = genreValues.findIndex(element => element[0] === genre);
    if (index === -1) {
      setGenres([...genreValues, [genre, value]]);
    } else {
      genreValues[index][1] = value;
      setGenres(genreValues);
    }
  }

  return (
    <>
      <h3 className='generate-h3'>Choose a picture</h3>
      <ul className='pics'>
        {pictureURLs.map(picture => {
          return (
            <li className='pic'>
              <img src={picture} alt="playlist" onClick={(e) => handleImage(e.target.src)} />
            </li>
          )
        })
        }
      </ul>
      <div className={classes.root}>
        {
          genres.map(genre => {
            return (
              <>
                <Typography key={genre} id="discrete-slider" gutterBottom>
                  {genre}
                </Typography>
                <Slider
                  name={genre}
                  aria-labelledby="discrete-slider"
                  valueLabelDisplay="off"
                  aria-label={genre}
                  step={10}
                  marks
                  min={0}
                  max={100}
                  defaultValue={genreValues.findIndex(element => element[0] === genre) !== -1
                    ? genreValues[genreValues.findIndex(element => element[0] === genre)][1] : 0}
                  onChange={(evt, value) => handleGenres(evt, genre, value)}
                />
              </>
            )
          })
        }
      </div>
    </>
  )
}

export default ThirdForm;
