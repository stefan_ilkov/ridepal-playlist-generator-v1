import React, { useState, useEffect } from 'react';
import './TopThreePlaylists.css';
import Loading from '../../Pages/Loading/Loading';
import AppError from '../../Pages/AppError/AppError';
import { BASE_URL } from '../../../common/constants';
import { Link } from 'react-router-dom';

const TopThreePlaylists = () => {
  const [topThreePlaylists, setTopThreePlaylists] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/playlists/topthree`)
      .then((response) => response.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setTopThreePlaylists(data);
        } else {
          throw new Error(data.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  if (loading) {
    return (
      <Loading>
        <h1>Loading playlists...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <ul className='top-playlists'>
      {topThreePlaylists.map((playlist) => {
        return (
          <li key={playlist.id} className='top-playlist'>
            <Link to={`/playlists/${playlist.id}`}>
              <img src={playlist.pic} alt='img' className='img-playlist'></img>
            </Link>
            <p><b>Playlist title: </b> {playlist.title}</p>
            <p><b>Duration: </b>{Math.floor((playlist.duration / 60))} min</p>
            <p><b>Average rank: </b> {playlist.rank}</p>
            {/* <p><b>Number of tracks: </b> {playlist.tracks}</p> */}
          </li>
        )
      })}
    </ul>
  )
}

export default TopThreePlaylists;
