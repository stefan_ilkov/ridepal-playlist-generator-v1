/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { BASE_URL } from '../../common/constants';
import { getToken } from '../../providers/AuthContext';


const EditPlaylist = ({ id }) => {
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState('');
  const [error, setError] = useState(null);

  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const editPlaylist = () => {
    fetch(`${BASE_URL}/playlists/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({ title: title }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        alert('Successfuly updated!');
      })
      .catch((error) => setError(error.message))

    setOpen(false);
    history.push(`/playlists`)
  };

  return (
    <>
      <Button color='primary' padding='0' className='playlist-edit-btn' onClick={handleClickOpen}>
        <FaEdit />
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby='form-dialog-title'>
        <DialogTitle id='form-dialog-title'>Edit playlist</DialogTitle>
        <DialogContent>
          <DialogContentText>
            You can only edit the title of the playlist
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='Enter new title'
            type='text'
            onChange={e => setTitle(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='primary'>
            Cancel
          </Button>
          <Button onClick={editPlaylist} color='primary'>
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default EditPlaylist;
