/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { RiDeleteBin5Fill } from 'react-icons/ri';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { BASE_URL } from '../../common/constants';
import { getToken } from '../../providers/AuthContext';

const DeletePlaylist = ({ id }) => {
  const [open, setOpen] = useState(false);
  const [error, setError] = useState(null);

  const history = useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const deletePlaylist = () => {

    fetch(`${BASE_URL}/playlists/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        alert('Successfuly deleted!');
      })
      .catch((error) => setError(error.message))

      setOpen(false);
      history.push(`/home`)

  }

  return (
    <>
      <Button color='primary' className='playlist-delete-btn' onClick={handleClickOpen}>
        <RiDeleteBin5Fill />
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>{'Are you sure you want to delete the playlist?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id='alert-dialog-description'>
            Once playlist is deleted, it can't be recovered.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='primary'>
            Cancel
          </Button>
          <Button onClick={() => deletePlaylist()} color='primary' autoFocus>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default DeletePlaylist;
