import React, { useState, useEffect } from 'react';
import './AllPlaylists.css';
import Loading from '../../Pages/Loading/Loading';
import AppError from '../../Pages/AppError/AppError';
import { BASE_URL } from '../../../common/constants';
import RenderPlaylists from './RenderPlaylists';


const AllPlaylists = () => {
  const [playlists, setPlaylists] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);


  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/playlists`)
      .then((response) => response.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setPlaylists(data);
        } else {
          throw new Error(data.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
    }, []);
    

  if (loading) {
    return (
      <Loading>
        <h1>Loading playlists...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <main className='main-all-playlists'>
      <h1>Playlists</h1>
      <RenderPlaylists playlists={playlists} />
    </main>
  )
}

export default AllPlaylists;
