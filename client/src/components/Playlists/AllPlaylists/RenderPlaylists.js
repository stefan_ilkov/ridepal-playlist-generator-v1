import React, { useContext } from 'react';
import './AllPlaylists.css';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AuthContext from '../../../providers/AuthContext';

const RenderPlaylists = ({ playlists }) => {


  const auth = useContext(AuthContext);

  return (
    <ul className='playlists'>
      {playlists.map((playlist) => {
        return (
          <li key={playlist.id} className='playlist'>
            <Link to={`/playlists/${playlist.id}`}>
              <img src={playlist.pic} alt='img' className='img-playlist'></img>
            </Link>
            {auth.isLoggedIn ?
              <>
                <p><b>Playlist title: </b> {playlist.title}</p>
                <p><b>Genres: </b> {playlist.genres}</p>
                <p><b>Duration: </b>{Math.floor((playlist.duration / 60))} min</p>
                <p><b>Average rank: </b> {playlist.rank}</p>
                <p><b>Number of tracks: </b> {playlist.tracks}</p>
              </>
              :
              <>
                <p><b>Playlist title: </b> {playlist.title}</p>
                <p><b>Duration: </b>{Math.floor((playlist.duration / 60))} min</p>
                <p><b>Average rank: </b> {playlist.rank}</p>
              </>
            }
          </li>
        )
      })}
    </ul>
  )
}

RenderPlaylists.propTypes = {
  playlists: PropTypes.array,
};

export default RenderPlaylists;
