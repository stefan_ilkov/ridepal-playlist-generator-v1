import React, { useState, useEffect, useContext } from 'react';
import './AllPlaylists.css';
import Loading from '../../Pages/Loading/Loading';
import AppError from '../../Pages/AppError/AppError';
import { BASE_URL } from '../../../common/constants';
import RenderPlaylists from './RenderPlaylists';
import AuthContext from '../../../providers/AuthContext';

const MyPlaylists = () => {
  const [playlists, setPlaylists] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const { user } = useContext(AuthContext);
  const userId = user.user_id;

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/playlists`)
      .then((response) => response.json())
      .then((data) => {
        if (Array.isArray(data)) {
          const result = data.filter(el => el.user_id === userId)
          setPlaylists(result);
        } else {
          throw new Error(data.message);
        }
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  if (loading) {
    return (
      <Loading>
        <h1>Loading playlists...</h1>
      </Loading>
    );
  }

  if (error) {
    return <AppError message={error} />;
  }

  return (
    <main className='main-all-playlists'>
      {playlists.length > 0 ? (
        <RenderPlaylists playlists={playlists} />
      ) : (
        <h3>No playlists to show</h3>
      )}
    </main >
  )
}

export default MyPlaylists;
