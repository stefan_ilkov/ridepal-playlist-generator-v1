import './App.css';
import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Header from './components/Base/Header/Header';
import Footer from './components/Base/Footer/Footer';
import Home from './components/Pages/Home/Home';
import NotFound from './components/Pages/NotFound/NotFound';
import AllPlaylists from './components/Playlists/AllPlaylists/AllPlaylists';
import Login from './components/Auth/Login/Login';
import GeneratePlaylist from './components/Playlists/GeneratePlaylist/GeneratePlaylist';
import Register from './components/Auth/Register/Register';
import GuardedRoute from './providers/GuardedRoute';
import AuthContext, { getUser } from './providers/AuthContext';
import AdminPage from './components/Pages/Admin/AdminPage';
import Playlist from './components/Playlists/PlaylistDetails/Playlist';
import AccountPage from './components/Pages/Account/AccountPage';


function App() {

  const [authValue, setAuthValue] = useState({
    user: getUser(),
    isLoggedIn: Boolean(getUser()),
  });

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ ...authValue, setAuthValue }}>
        <Header />
        <Switch>
          <Redirect path='/' exact to='/home' />
          <Route path='/home' exact component={Home} />
          <Route path='/playlists' isLoggedIn={authValue.isLoggedIn} exact component={AllPlaylists} />
          <GuardedRoute path='/account' isLoggedIn={authValue.isLoggedIn} exact component={AccountPage} />
          <Route path='/playlists/:id' exact component={Playlist} />
          <Route path='/admin' exact component={AdminPage} />
          <Route path='/generation' exact component={GeneratePlaylist} />
          <Route path='/login' exact component={Login} />
          <Route path='/signup' exact component={Register} />
          <Route path='*' exact component={NotFound} />
        </Switch>
        <Footer />
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
