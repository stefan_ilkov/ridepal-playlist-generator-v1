export const genreIds = {
  ROCK: 152,
  POP: 132,
  RAP: 116,
  JAZZ: 129,
  REGGAE: 144,
  R_AND_B: 165,
  LATIN_MUSIC: 197,
  ALTERNATIVE: 85,
  ELECTRO: 106,
  CLASSICAL: 98,
};
